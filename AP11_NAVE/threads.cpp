#include "threads.h"

using std::cout;
using std::endl;

void I_Love_Threads()
{
	cout << "I Love Threads" << endl;
}

void call_I_Love_Threads()
{
	std::thread thread(I_Love_Threads);
	thread.join();
}

bool isPrime(int num)
{
	int end = sqrt(num); //efficenecy!!!
	for (int i = 2; i <= end; i++)
		if (num % i == 0)
			return false;
	return true;
}

void getPrimes(int begin, int end, std::vector<int>& primes)
{
	for (int i = begin; i < end; i++)
		if (isPrime(i))
			primes.push_back(i);
}

void printVector(std::vector<int> primes)
{
	std::copy(primes.begin(), primes.end(), std::ostream_iterator<int>(cout, "\n"));
}

std::vector<int> callGetPrimes(int begin, int end)
{
	std::vector<int> primes;
	clock_t start = clock();
	std::thread thread2(getPrimes, begin, end, std::ref(primes));
	thread2.join();
	clock_t finish = clock();
	cout << "getPrimes() ran  " << (finish - start) / (double)CLOCKS_PER_SEC << "s." << endl;
	return primes;
}

void writePrimesToFile(int begin, int end, std::ofstream& file)
{
	for (int i = begin; i <= end; i++)
		if (isPrime(i))
			file << i << ", ";
}

void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)
{
	std::ofstream file;
	file.open(filePath);
	int jump = (end - begin) / double(N);
	int i = 0;
	clock_t start = clock();
	for (i = begin; i <= end; i += jump + 1)
	{
		//cout << "thread " << i << endl;
		int endThread = (i + jump > end) ? end : i + jump;
		std::thread thread(writePrimesToFile, i, endThread, std::ref(file));
		thread.join();
	}
	clock_t finish = clock();
	cout << "callWritePrimesMultipleThreads() ran  " << (finish - start) / (double)CLOCKS_PER_SEC << "s." << endl;
	file.close();
}
