#include "threads.h"

int main()
{
	call_I_Love_Threads();

	std::vector<int> primes1;
	getPrimes(0, 100, primes1);	
	//printVector(primes1);

	std::vector<int> primes3 = callGetPrimes(93, 28900);
	//printVector(primes3);

	callWritePrimesMultipleThreads(0, 1000000, "primes2.txt", 2);

	system("pause");
	return 0;
}